class Game
  attr_reader :state
  private :state

  def initialize(state)
    @state = state
  end

  def self.start(state: State::Init)
    new(state).start
  end

  def start
    state.start
  end
end
