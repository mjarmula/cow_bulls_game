class Game
  class Menu
    def self.render
      new.render
    end

    def render
      puts delimiter
      puts "To quit press #{Game::UserInput::INTERRUPTION_SINGAL}"
      puts delimiter
    end

    private

    def delimiter
      '-----------------'
    end
  end
end
