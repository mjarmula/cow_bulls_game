class Game
  class NumberAnalyser
    module Strategies
      class Iteration
        attr_reader :init_number, :number_to_compare
        private :init_number, :number_to_compare

        def initialize(init_number, number_to_compare)
          @init_number = init_number
          @number_to_compare = number_to_compare
        end

        def self.check(init_number, number_to_compare)
          new(init_number, number_to_compare).check
        end

        def check
          [].tap do |result|
            number_to_compare.each_with_index do |number, index|
              if numbers_exist_on_the_same_position?(number, index)
                result << Game::NumberAnalyser::Cow
              else
                next unless init_number.include?(number)

                result << Game::NumberAnalyser::Bull
              end
            end
          end
        end

        private

        def numbers_exist_on_the_same_position?(number, index)
          init_number[index] == number
        end
      end
    end
  end
end
