class Game
  class NumberAnalyser
    class Strategy
      attr_reader :type
      private :type

      def initialize(type)
        @type = type
      end

      def self.fetch(type)
        new(type).fetch
      end

      def fetch
        defined_types.fetch(type)
      end

      private

      def defined_types
        {
          # Consumes more memory usage but is faster O(n)
          memory: Game::NumberAnalyser::Strategies::Memory,
          # Consumes less memory usage but is slower O(n2)
          iteration: Game::NumberAnalyser::Strategies::Iteration
        }
      end
    end
  end
end
