class Game
  class NumberAnalyser
    Cow = 'cow'
    Bull = 'bull'
    MaxNumber = 4

    attr_reader :init_number, :number_to_compare, :type, :result
    private :init_number, :number_to_compare, :type, :result

    def initialize(init_number, number_to_compare, type)
      @init_number = init_number.split('')
      @number_to_compare = number_to_compare.split('')
      @type = type
    end

    def self.check(init_number:, number_to_compare:, type: :memory)
      new(init_number, number_to_compare, type).check
    end

    def check
      self.tap do
        @result = strategy.check(init_number, number_to_compare)
      end
    end

    def cows_number
      @cows_number = result.select { |item| item == Game::NumberAnalyser::Cow }.size
    end

    def bulls_number
      @bulls_number = result.select { |item| item == Game::NumberAnalyser::Bull }.size
    end

    def success?
      cows_number == Game::NumberAnalyser::MaxNumber
    end

    private

    def strategy
      Game::NumberAnalyser::Strategy.fetch(type)
    end
  end
end
