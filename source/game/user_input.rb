class Game
  class UserInput
    INTERRUPTION_SINGAL = 'q'

    InvalidNumber = Class.new(StandardError)
    Interruption = Class.new(StandardError)

    attr_reader :input
    private :input

    def initialize(input)
      @input = input
    end

    def self.number!(input)
      new(input).number!
    end

    def number!
      validate_number!
      input
    end

    private

    def validate_number!
      raise Game::UserInput::Interruption if input == Game::UserInput::INTERRUPTION_SINGAL
      raise Game::UserInput::InvalidNumber unless valid_number?
    end

    def valid_number?
      input =~ /\b\d{#{max_size},#{max_size}}\b/
    end

    def max_size
      Game::NumberAnalyser::MaxNumber
    end
  end
end
