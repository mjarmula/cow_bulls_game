require 'singleton'

class Helpers
  include Singleton

  def prompter
    @prompter ||= Helpers::Prompter.new
  end
end
