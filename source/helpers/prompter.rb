class Helpers
  class Prompter
    def gets(message, source: STDIN)
      puts message
      source.gets.chomp
    end
  end
end
