module State
  class End
    def self.start
      new.start
    end

    def start
      puts '4 cows, congratulations!'
    end
  end
end
