module State
  class Init
    attr_reader :next_state
    private :next_state

    def initialize(next_state)
      @next_state = next_state
    end

    def self.start(next_state: State::Gameplay)
      new(next_state).start
    end

    def start
      render
      next_state.start
    end

    private

    def render
      puts '--==] Welcome to the Cows and Bulls game [==--'
    end
  end
end
