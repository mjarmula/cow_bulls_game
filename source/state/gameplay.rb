module State
  class Gameplay
    attr_reader :next_state, :init_number, :execute
    private :next_state, :init_number, :execute

    def initialize(next_state: State::End)
      @next_state = next_state
      @init_number = Faker::Number.number(digits: 4).to_s
      @execute = true
    end

    def self.start
      new.start
    end

    def start
      menu.render

      while execute do
        begin
          result = number_analyser.check(
            init_number: init_number,
            number_to_compare: user_input.number!
          )

          return next_state.start if result.success?

          puts "#{result.cows_number} cows, #{result.bulls_number} bulls"

        rescue Game::UserInput::InvalidNumber
          puts 'Given number has invalid format, please try again'
        rescue Game::UserInput::Interruption
          stop_loop
        end
      end
    end

    private

    def menu
      Game::Menu
    end

    def user_input
      Game::UserInput.new(prompter.gets('Your number: '))
    end

    def prompter
      Helpers.instance.prompter
    end

    def number_analyser
      Game::NumberAnalyser
    end

    def stop_loop
      @execute = false
    end
  end
end
