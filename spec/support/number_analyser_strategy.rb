RSpec.shared_examples 'number analyser strategy' do
  subject { described_class.check(init_number, number_to_compare) }

  context 'when a number matches by position' do
    let(:init_number) { %w[1 2 3 4] }
    let(:number_to_compare) {%w[1 8 8 8]}

    it 'returns one cow' do
      expect(subject).to eq(['cow'])
    end
  end

  context 'when a number matches by position and other appears in both sequences' do
    let(:init_number) { %w[1 2 3 4] }
    let(:number_to_compare) {%w[1 8 8 2]}

    it 'returns one cow, one bull' do
      expect(subject).to eq(['cow', 'bull'])
    end
  end

  context 'when numbers appear only in both sequences' do
    let(:init_number) { %w[1 2 3 4] }
    let(:number_to_compare) {%w[4 3 2 1]}

    it 'returns bulls' do
      expect(subject).to eq(['bull', 'bull', 'bull', 'bull'])
    end
  end

  context 'when all numbers match by position' do
    let(:init_number) { %w[1 2 3 4] }
    let(:number_to_compare) {%w[1 2 3 4]}

    it 'returns cows' do
      expect(subject).to eq(['cow', 'cow', 'cow', 'cow'])
    end
  end

  context 'when there is no matching number' do
    let(:init_number) { %w[1 2 3 4] }
    let(:number_to_compare) {%w[6 7 8 9]}

    it 'returns empty array' do
      expect(subject).to eq([])
    end
  end
end
