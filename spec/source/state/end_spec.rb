require 'spec_helper'

describe State::End do
  describe '.start' do
    let(:goodbye_message) { '4 cows, congratulations!' }

    it 'renders goodbye message' do
      expect(STDOUT).to receive(:puts).with(goodbye_message)

      described_class.start
    end
  end
end
