require 'spec_helper'

describe State::Gameplay do
  let(:number_analyser) { instance_double('Game::NumberAnalyser') }
  let(:user_input) { instance_double('Game::UserInput', number!: number) }
  let(:prompter) { instance_double('Helpers::Prompter') }
  let(:number) { '1234' }

  before do
    allow(Game::Menu).to receive(:render)
    allow(Game::NumberAnalyser).to receive(:check).and_return(number_analyser)
    allow(Game::UserInput).to receive(:new).and_return(user_input)
    allow(Helpers).to receive_message_chain(:instance, :prompter).and_return(prompter)
    allow(prompter).to receive(:gets)
  end

  context 'when the number is known' do
    before do
      allow(number_analyser).to receive(:success?).and_return(true)
    end

    it 'goes to the next state' do
      expect(State::End).to receive(:start)

      described_class.start
    end
  end

  context 'when the number is still unknown' do
    let(:cows_number) { 1 }
    let(:bulls_number) { 1 }
    let(:expected_message) { "#{cows_number} cows, #{bulls_number} bulls" }

    before do
      allow(number_analyser).to receive(:success?).and_return(false)
      allow(number_analyser).to receive(:cows_number).and_return(cows_number)
      allow(number_analyser).to receive(:bulls_number).and_return(bulls_number)
      allow_any_instance_of(described_class).to receive(:execute).and_return(true, false)
    end

    it 'goes to the next state' do
      expect(STDOUT).to receive(:puts).with(expected_message)

      described_class.start
    end
  end
end
