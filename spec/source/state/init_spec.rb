require 'spec_helper'

describe State::Init do
  let(:next_state) { instance_double('State::Gameplay') }
  let(:welcome_message) { '--==] Welcome to the Cows and Bulls game [==--' }

  describe '.start' do
    before do
      allow(STDOUT).to receive(:puts)
      allow(next_state).to receive(:start)
    end

    it 'renders welcome message' do
      expect(STDOUT).to receive(:puts).with(welcome_message)

      described_class.start(next_state: next_state)
    end

    it 'runs next state' do
      expect(next_state).to receive(:start)

      described_class.start(next_state: next_state)
    end
  end
end
