require 'spec_helper'

describe Game do
  let(:state) { instance_double('State::Init') }

  describe '.start' do
    it 'starts given state' do
      expect(state).to receive(:start)

      described_class.start(state: state)
    end
  end
end
