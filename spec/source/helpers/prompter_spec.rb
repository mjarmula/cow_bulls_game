require 'spec_helper'

describe Helpers::Prompter do
  subject(:instance) { described_class.new }

  describe '#gets' do
    let(:message) { 'Some dummy message' }
    let(:user_input) { '1234' }

    it 'returns user input' do
      expect(STDOUT).to receive(:puts).with(message)
      expect(STDIN).to receive(:gets).and_return(user_input)

      instance.gets(message)
    end
  end
end
