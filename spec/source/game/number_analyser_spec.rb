require 'spec_helper'

describe Game::NumberAnalyser do
  let(:type) { :memory }
  let(:set_of_data) { ['cow', 'bull', 'bull'] }
  let(:init_number) { '1234' }
  let(:number_to_compare) { '1419' }
  let(:init_number_converted) { %w[1 2 3 4] }
  let(:number_to_compare_converted) { %w[1 4 1 9] }
  let(:number_analyser_strategy) { class_double('Game::NumberAnalyser::Strategies::Memory') }

  before do
    allow(Game::NumberAnalyser::Strategy).to receive(:fetch).with(type).
      and_return(number_analyser_strategy)
    allow(number_analyser_strategy).to receive(:check).and_return(set_of_data)
  end

  describe '.check' do
    it 'performs check on strategy' do
      expect(number_analyser_strategy).to receive(:check).
        with(init_number_converted, number_to_compare_converted)

      described_class.check(init_number: init_number, number_to_compare: number_to_compare)
    end
  end

  describe '#cows_number' do
    subject(:instance) { described_class.check(init_number: init_number, number_to_compare: number_to_compare) }

    context 'where there are cows' do
      it 'returns cows number' do
        expect(instance.cows_number).to eq(1)
      end
    end

    context 'where there are no cows' do
      let(:set_of_data) { ['bull'] }

      it 'returns 0' do
        expect(instance.cows_number).to eq(0)
      end
    end
  end

  describe '#bulls_number' do
    subject(:instance) { described_class.check(init_number: init_number, number_to_compare: number_to_compare) }

    context 'where there are bulls' do
      it 'returns bulls number' do
        expect(instance.bulls_number).to eq(2)
      end
    end

    context 'where there are no bulls' do
      let(:set_of_data) { ['cow'] }

      it 'returns 0' do
        expect(instance.bulls_number).to eq(0)
      end
    end
  end

  describe '#success?' do
    subject(:instance) { described_class.check(init_number: init_number, number_to_compare: number_to_compare) }

    context 'when there is less than 4 cows' do
      let(:set_of_data) { ['cow', 'cow', 'cow', 'bull'] }

      it 'returns false' do
        expect(instance.success?).to eq(false)
      end
    end

    context 'when there are 4 cows' do
      let(:set_of_data) { ['cow', 'cow', 'cow', 'cow'] }

      it 'returns true' do
        expect(instance.success?).to eq(true)
      end
    end
  end
end
