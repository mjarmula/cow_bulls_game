require 'spec_helper'

describe Game::Menu do
  let(:delimiter) { '-----------------' }
  describe '.render' do
    it 'renders menu' do
      expect(STDOUT).to receive(:puts).twice.with(delimiter)
      expect(STDOUT).to receive(:puts).with('To quit press q')

      described_class.render
    end
  end
end
