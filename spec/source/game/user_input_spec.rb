require 'spec_helper'

describe Game::UserInput do
  describe '.number!' do
    context 'when the input has incorrect format' do
      let(:input) { '123dsa' }

      it 'throws invalid number error' do
        expect { described_class.number!(input) }.to raise_error(Game::UserInput::InvalidNumber)
      end
    end

    context 'when the input is too short' do
      let(:input) { '123' }

      it 'throws invalid number error' do
        expect { described_class.number!(input) }.to raise_error(Game::UserInput::InvalidNumber)
      end
    end

    context 'when the input is too long' do
      let(:input) { '12345' }

      it 'throws invalid number error' do
        expect { described_class.number!(input) }.to raise_error(Game::UserInput::InvalidNumber)
      end
    end

    context 'when the input is interrupt signal' do
      let(:input) { 'q' }

      it 'throws invalid number error' do
        expect { described_class.number!(input) }.to raise_error(Game::UserInput::Interruption)
      end
    end

    context 'when the input has correct format' do
      let(:input) { '1234' }

      it 'returns input' do
        expect(described_class.number!(input)).to eq(input)
      end
    end
  end
end
