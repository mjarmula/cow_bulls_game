require 'spec_helper'

describe Game::NumberAnalyser::Strategy do
  describe '.fetch' do
    context 'when strategy is memory consuming' do
      let(:type) { :memory }

      it 'returns memory consuming strategy' do
        expect(described_class.fetch(type)).to eq(Game::NumberAnalyser::Strategies::Memory)
      end
    end

    context 'when strategy is computing consuming' do
      let(:type) { :iteration }

      it 'returns computing consuming strategy' do
        expect(described_class.fetch(type)).to eq(Game::NumberAnalyser::Strategies::Iteration)
      end
    end
  end
end
