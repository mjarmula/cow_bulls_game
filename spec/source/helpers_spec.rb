require 'spec_helper'

describe Helpers do
  describe '#prompter' do
    it 'returns prompter' do
      expect(described_class.instance.prompter).to be_a(Helpers::Prompter)
    end
  end
end
