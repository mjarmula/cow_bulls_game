# Cows and bulls game
Napisz grę "cows and bulls", której zasady są następujące: na początku losowany jest
ciąg 4 cyfr, np. 1549, następnie użytkownik wpisując swoje 4ro cyfrowe ciągi próbuje
zgadnąć wynik, przy czym dostaje informacje zwrotne: każda pozycja, dla której cyfry się
zgadzają oznacza 1 krowę, a dla każdej cyfry, która występuje w obu ciągach, ale na
różnych pozycjach wypisujemy byka. Wygrana jest wtedy, gdy są cztery krowy.
## Getting Started
### Prerequisites
ruby >= 2.5.1

### How to run
```
ruby main.rb
```

### How to test
```
rspec
```
